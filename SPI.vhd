----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    07:56:16 01/12/2021 
-- Design Name: 
-- Module Name:    SPI - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;
use IEEE.NUMERIC_STD.ALL;

use work.BootLoaderPkg.all;
-------------------------------------------------------------

entity SPI is
	Port 
	( 
		i_clk					:	in			STD_LOGIC;
		CS 					:	out  		STD_LOGIC;
		CLK 					:	out  		STD_LOGIC;
		SI 					:	out  		STD_LOGIC;
		SO 					:	in  		STD_LOGIC;
		
		GET_RECORD			:	in			std_logic;
		RECORD_OK			: 	out		std_logic;
		READ_COMPLETE		: 	out		std_logic;
		
		BOOT_FLAGS			:	out		std_logic_vector(7 downto 0);
		BOOT_PAGE			:	out		std_logic_vector(23 downto 0);
		FALLBACK_PAGE		:	out		std_logic_vector(23 downto 0)
	);
end SPI;
-----------------------------------------------------

--//////////////////////////////////////////////////////////////////////////
--
-- Architecture
--
--//////////////////////////////////////////////////////////////////////////

architecture Behavioral of SPI is

--//////////////////////////////////////////////////////////////////////////
--
-- Signals
--
--//////////////////////////////////////////////////////////////////////////

signal	FlashCS:					std_logic																:= '0';
signal	FlashSI:					std_logic																:= '0';
signal	FlashSO:					std_logic																:= '0';
signal	FlashCLK:				std_logic																:= '0';

signal 	RecordPage:				integer range 0 to 4096												:= RECORD_PAGE;
signal	RecordOK:				std_logic																:= '0';
signal	ReadComplete:			std_logic																:= '0';

signal 	BootFlags:				std_logic_vector(7 downto 0)										:=X"00"; 
signal   BootPage    : 			std_logic_vector(23 downto 0)										:=X"000000";  
signal   FallbackPage: 			std_logic_vector(23 downto 0)										:=X"000000"; 

signal 	Page:						std_logic_vector(((PAGE_SIZE_BYTES * 8) - 1) downto 0)	:= (others => '0'); -- 528 bytes 
signal	PageIndex:				integer range 0 to (PAGE_SIZE_BYTES * 8)						:= 0;

type 		FLASH_STATE_TYPE		is																			(IDLE, INIT_SPI, SEND_0B_CMD, READ_PAGE, DONE);
signal	FlashState:				FLASH_STATE_TYPE														:= IDLE;

signal 	FlashCmdIndex:			integer range 0 to FLASH_READ_CMD'length						:= 0;


--//////////////////////////////////////////////////////////////////////////
--
-- BEGIN
--
--//////////////////////////////////////////////////////////////////////////

begin

	CS <= not FlashCS;
	SI <= FlashSI;
	CLK <= FlashCLK;
	FlashSO <= SO;
	
	RECORD_OK <= RecordOK;
	READ_COMPLETE <= ReadComplete;
	
	BOOT_FLAGS <= BootFlags;
	BOOT_PAGE <= BootPage;
	FALLBACK_PAGE <= FallbackPage;

--//////////////////////////////////////////////////////////////////////////
--
-- processes
--
--//////////////////////////////////////////////////////////////////////////
	
	FLASH_PROCESS: process (i_clk)
	begin
		if rising_edge (i_clk) then
			
			case FlashState is
			
				when IDLE =>
					FlashCS <= '0';
					FlashCLK <= '0';
					if (GET_RECORD = '1') then
						ReadComplete <= '0';
						RecordOK <= '0';
						PageIndex <= 0;
						Page <= (others => '0');
						FlashCmdIndex <= 0;
						FlashState <= INIT_SPI;
					end if;
					
				when INIT_SPI =>
					FlashCS <= '1';
					FlashSI <= FLASH_READ_CMD(FlashCmdIndex);
					FlashState <= SEND_0B_CMD;
					
				when SEND_0B_CMD =>
					if (FlashCLK = '1') then
						FlashSI <= FLASH_READ_CMD(FlashCmdIndex);
						FlashCLK <= '0';
					elsif (FlashCmdIndex = FLASH_READ_CMD'length) then
						FlashState <= READ_PAGE;
					else
						FlashClk <= '1';
						FlashCmdIndex <= FlashCmdIndex + 1;
					end if;
					
				when READ_PAGE =>
					if (FlashCLK = '1') then
						Page(PageIndex) <= FlashSO;
						PageIndex <= PageIndex + 1;
						FlashCLK <= '0';
					elsif (PageIndex = PAGE_SIZE_BYTES * 8) then
						FlashState <= DONE;
					else
						FlashClk <= '1';
					end if;
					
				when DONE =>
					FlashCS <= '0';
					if (Page(7 downto 0) /= X"FF") then
						BootFlags <= Page(7 downto 0);
						BootPage <= Page(31 downto 8);
						FallbackPage <= Page(55 downto 32);
						RecordOK <= '1';
					end if;
					ReadComplete <= '1';
					FlashState <= IDLE;
			
			end case;
		
		end if;
	end process FLASH_PROCESS;

end Behavioral;
-------------------------------------------------------------------

