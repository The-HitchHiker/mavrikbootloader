 ----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 			The HitchHiker
-- 
-- Create Date:    10:29:09 12.01.21 
-- Design Name: 
-- Module Name:    MavrikBootLoader - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
----
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;
use IEEE.NUMERIC_STD.ALL;

use work.BootLoaderPkg.all;
----------------------------------------------------------------------------------


entity MavrikBootLoader is
	port
	(
			m_clock : 				in 				std_logic ; -- system clock (100 MHz)

			FLASH_CS:				out				std_logic;
			FLASH_SCK:				out				std_logic;
			FLASH_SI:				out				std_logic;
			FLASH_SO:				in					std_logic
	);
end MavrikBootLoader;
----------------------------------------------------------------------------------


--//////////////////////////////////////////////////////////////////////////
--
-- Architecture
--
--//////////////////////////////////////////////////////////////////////////

architecture Behavioral of MavrikBootLoader is

	
--====== Version	=================	
--=================================
 
constant VERSION: integer := 0; 
constant BUILD: integer := 0;			
constant REVISION: integer := 1;
												-- 1: First version


--//////////////////////////////////////////////////////////////////////////
--
-- Components
--
--//////////////////////////////////////////////////////////////////////////

component MainClock_65Mhz
	port
	 (  
		CLK_IN1           :	in			std_logic;-- Clock in ports
		CLK_OUT1          :	out		std_logic-- Clock out ports
	 );
	end component;
	----------------------------------------------------------

component SPI 
	Port 
	( 
		i_clk					:	in			STD_LOGIC;
		CS 					:	out  		STD_LOGIC;
		CLK 					:	out  		STD_LOGIC;
		SI 					:	out  		STD_LOGIC;
		SO 					:	in  		STD_LOGIC;		
		
		GET_RECORD			:	in			std_logic;
		RECORD_OK			: 	out		std_logic;
		READ_COMPLETE		: 	out		std_logic;
		
		BOOT_FLAGS			:	out		std_logic_vector(7 downto 0);
		BOOT_PAGE			:	out		std_logic_vector(23 downto 0);
		FALLBACK_PAGE		:	out		std_logic_vector(23 downto 0)
	);
	end component;
	-----------------------------------------------------

--//////////////////////////////////////////////////////////////////////////
--
-- Signals & constants
--
--//////////////////////////////////////////////////////////////////////////

signal 		clock: 				std_logic ; --internal clock

signal    	icap_d      : 		std_logic_vector(15 downto 0);
signal    	bit_swapped : 		std_logic_vector(15 downto 0) 		:=X"FFFF";
signal    	icap_d_o      : 	std_logic_vector(15 downto 0);
signal    	bit_swapped_o : 	std_logic_vector(15 downto 0) 		:=X"FFFF";
signal    	icap_cs     : 		std_logic 									:= '1';
signal    	icap_rw     : 		std_logic 									:= '1';
constant  	CCOUNT      : 		integer 										:= 30;
signal    	cnt_bitst   : 		integer range 0 to CCOUNT 				:= 0;

signal    	BootPageR    : 	std_logic_vector(23 downto 0)			:=X"000000";
signal    	FallbackPageR: 	std_logic_vector(23 downto 0)			:=X"000000";
signal 	 	NeedToLoadRegs:	std_logic									:='1';


signal 		BootFlags:			std_logic_vector(7 downto 0)			:=X"00"; 
-- 0		:	new boot valifated (turns to 1 by FW)
-- 1..2	:	boot retries (count by bootloader trying to load from new boot until validated, if =3 load from fallback)

signal    	RecordBootPage :		 	std_logic_vector(23 downto 0)			:=X"000000";  -- if BootFlag(0)='1'
signal    	RecordFallbackPage: 		std_logic_vector(23 downto 0)			:=X"000000";  -- if BootFlag(0)='0' and BootFlag(2 downto 1)=X"3"
signal    	BootPage    : 		std_logic_vector(23 downto 0)			:=X"000000";  -- if BootFlag(0)='1'
signal    	FallbackPage: 		std_logic_vector(23 downto 0)			:=X"000000";  -- if BootFlag(0)='0' and BootFlag(2 downto 1)=X"3"
signal		BootValidated:		std_logic									:= '0';
signal 		BootRetries:		integer range 0 to 3						:= 0;

signal		GetRecordCMD:		std_logic									:= '0';
signal		RebootCMD:			std_logic									:= '0';
signal		RecordOK:			std_logic									:= '0';
signal		ReadComplete:		std_logic									:= '0';

type			BootloaderState_type is											(IDLE, INIT, WAIT_READ_RECORD, SET_REBOOT_PARAMS, REBOOT);
signal 		BootloaderState:	BootloaderState_type						:= IDLE;


--//////////////////////////////////////////////////////////////////////////
--
-- BEGIN
--
--//////////////////////////////////////////////////////////////////////////

begin

--//////////////////////////////////////////////////////////////////////////
--
-- port mapping
--
--//////////////////////////////////////////////////////////////////////////

	SystemClock : MainClock_65Mhz
		port map
		(	
			CLK_IN1            => m_clock,
			CLK_OUT1           => clock
		);
	------------------------------------------------------------
	
	FlashSPI	:	SPI
		port map
		(
			i_clk					=> clock,
			CS 					=> FLASH_CS,
			CLK 					=> FLASH_SCK,
			SI 					=> FLASH_SI,
			SO 					=> FLASH_SO,
		
			GET_RECORD			=> GetRecordCMD,
			RECORD_OK			=> RecordOK,
			READ_COMPLETE		=> ReadComplete,
			
			BOOT_FLAGS			=> BootFlags,
			BOOT_PAGE			=> RecordBootPage,
			FALLBACK_PAGE		=> RecordFallbackPage
		);
	------------------------------------------------------------
	
	ICAP_SPARTAN6_inst : ICAP_SPARTAN6
	generic map 
	(
		DEVICE_ID => X"4000093", -- Specifies the pre-programmed Device ID value
		SIM_CFG_FILE_NAME => "NONE" -- Specifies the Raw Bitstream (RBT) file to be parsed by the simulation
	)
	port map 
	(
		BUSY => open, -- 1-bit output: Busy/Ready output
		O => bit_swapped_o, -- 16-bit output: Configuartion data output bus
		CE => icap_cs, -- 1-bit input: Active-Low ICAP Enable input
		CLK => clock, -- 1-bit input: Clock input
		I => bit_swapped, -- 16-bit input: Configuration data input bus
		WRITE => icap_rw -- 1-bit input: Read/Write control input
	);
	-----------
	
--//////////////////////////////////////////////////////////////////////////
--
-- processes
--
--//////////////////////////////////////////////////////////////////////////
	
	BootValidated <= BootFlags(0);
	
	BOOTLOADER_MAIN: process (clock)
	begin
		if rising_edge (clock) then
		
			case BootloaderState is
			
				when IDLE =>
					GetRecordCMD <= '0';
					RebootCmd <= '0';
					BootloaderState <= INIT;
					
				when INIT =>
					GetRecordCMD <= '1';
					BootloaderState <= WAIT_READ_RECORD;
					
				when WAIT_READ_RECORD =>
					if (ReadComplete = '1') then
						if (RecordOK = '1') then
							BootloaderState <= SET_REBOOT_PARAMS;
						else
							BootloaderState <= IDLE;
						end if;
					end if;
					
				when SET_REBOOT_PARAMS =>
					if (BootValidated = '1') then
						BootPage <= RecordBootPage;
						FallbackPage <= RecordFallbackPage;
					else
						BootPage <= RecordFallbackPage;
						FallbackPage <= RecordFallbackPage;
					end if;
					BootloaderState <= REBOOT;
					
				when REBOOT =>
					RebootCmd <= '1';
			
			end case;
			
			
			if (RebootCmd = '1') then
				
				if(cnt_bitst /= CCOUNT)  then
						cnt_bitst <= cnt_bitst + 1;
					end if;

			  case cnt_bitst is
					when  0 => icap_cs <= '0'; icap_rw <= '0';
					-- using registers for now
					when  1 => icap_d <= x"FFFF";   -- Dummy Word
					when  2 => icap_d <= x"AA99";   -- Sync Word
					when  3 => icap_d <= x"5566";   -- Sync Word
					when  4 => icap_d <= x"3261";   -- Type 1 Write 1 Words to GENERAL_1
					when  5 => icap_d <= BootPage(15 downto 0); --x"5566";   -- MultiBoot Start Address [15:0]
					when  6 => icap_d <= x"3281";   -- Type 1 Write 1 Word to GENERAL2
					when  7 => icap_d <= x"0B" & BootPage(23 downto 16);   -- Opcode and MultiBoot Start Address [23:16]
					when  8 => icap_d <= x"32A1";   -- Type 1 Write 1 Word to GENERAL3
					when  9 => icap_d <= FallbackPage(15 downto 0); --x"5566";   -- Fallback Start Address [15:0]
					when  10 => icap_d <= x"32C1";   -- Type 1 Write 1 Word to GENERAL4
					when  11 => icap_d <= x"0B" & FallbackPage(23 downto 16);   -- Opcode and Fallback Start Address [23:16]
					when  12 => icap_d <= x"30A1";   -- Type 1 Write 1 Words to CMD
					when  13 => icap_d <= x"000E";   -- IPROG Command
					when  14 => icap_d <= x"2000";   -- Type 1 NO OP
					when  15 => icap_d <= x"2000";   -- Type 1 NO OP
					when  16 => icap_d <= x"2000";   -- Type 1 NO OP
					-- Bye, bye
					when others => icap_cs <= '1'; icap_rw <= '1';
			  end case;
				
			else			
				icap_cs  <= '1';
				icap_rw  <= '1';
				cnt_bitst  <= 0;
				
			end if; -- reboot
			
		end if; -- clock
	end process BOOTLOADER_MAIN;	
	
	bit_swapped(15 downto 8)  <= icap_d(8) & icap_d(9) & icap_d(10) & icap_d(11) & icap_d(12) & icap_d(13) & icap_d(14) & icap_d(15);
	bit_swapped(7 downto 0)   <= icap_d(0) & icap_d(1) & icap_d(2) & icap_d(3) & icap_d(4) & icap_d(5) & icap_d(6) & icap_d(7);
		
end Behavioral;
